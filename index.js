const express = require('express')
const app = express()
const PORT = 5000 || process.env.PORT
const dotenv = require('dotenv')
const { MongoClient, ServerApiVersion } = require('mongodb');
const cors= require('cors')
const bcrypt = require('bcryptjs');
const bodyParser = require('body-parser');

// middleware 
dotenv.config()
app.use(express.json());
app.use(cors());
app.use(bodyParser.json());

 
const uri = `mongodb+srv://${process.env.DB_USER}:${process.env.DB_PASS}@cluster0.o1ht6xv.mongodb.net/?retryWrites=true&w=majority&appName=Cluster0`;



// Create a MongoClient with a MongoClientOptions object to set the Stable API version
const client = new MongoClient(uri, {
  serverApi: {
    version: ServerApiVersion.v1,
    strict: true,
    deprecationErrors: true,
  }
});

async function run() {
  try {


    const  wBackend= client.db("W_Backend");
    const  usersCollection = wBackend.collection('users')

    //Sign Up api
    app.post('/signUp', async(req,res)=>{
        const { email, password } = req.body;
        console.log(email, password);

        // // Check if email already exists
        const existingUser = await usersCollection.findOne({ email: email });
        if (existingUser) {
            return res.status(400).json({ error: 'Email already exists' });
        }
    
        try {
            // Encrypt the password
            const hashedPassword = await bcrypt.hash(password, 10);
            
            // Create new user
            const newUser = { email, password: hashedPassword ,role:'member'};
           const result = await usersCollection.insertOne(newUser);
            // res.status(201).json({ message: 'User created successfully', user: { email: newUser.email } });
            res.send(result)
            console.log(result);
            
        } catch (error) {
            console.log(error.message);
            // res.status(500).json({ error: 'Internal server error' });
        }
    })

    // Login route
    app.post('/login', async (req, res) => {
        const { email, password } = req.body;
    
        try {
            // Find user by email
            const user = await usersCollection.findOne({ email: email });
            if (!user) {
                return res.status(401).json({ error: 'User not found' });
            }
    
            // Check password
            const passwordMatch = await bcrypt.compare(password, user.password);
            if (!passwordMatch) {
                return res.status(401).json({ error: 'Invalid password' });
            }
            
            // Password is correct, user is authenticated
            res.status(200).json({ message: 'Login successful', user  });

        } catch (error) {
            console.error(error.message);
            res.status(500).json({ error: 'Internal server error' });
        }
    });

    app.get('/usersData', async(req,res)=>{
        const result = await usersCollection.find().toArray()
        console.log(result);
        res.send(result)
    })



    // Connect the client to the server	(optional starting in v4.7)
    await client.connect();
    // Send a ping to confirm a successful connection
    await client.db("admin").command({ ping: 1 });
    console.log("Pinged your deployment. You successfully connected to MongoDB!");
  } finally {
    // Ensures that the client will close when you finish/error
    // await client.close();
  }
}
run().catch(console.dir);



app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.listen(PORT, () => {
  console.log(`Server running on PORT ${PORT}`)
})